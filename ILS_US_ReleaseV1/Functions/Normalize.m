function FTS = Normalize(FTS,normalization)

switch normalization
    case 'direct'
        disp('No normalization performed...')
    case 'zscore'
        FTS = zscore(FTS,1);
    case 'relu'
        FTS = FTS.*(FTS > 0);
    case 'relu_l1'
        FTS = FTS.*(FTS > 0);
        FTS = FTS ./ repmat(sum(abs(FTS),2),1,size(FTS,2));
    case 'relu_l1_sqrt'
        FTS = FTS.*(FTS > 0);
        FTS = FTS ./ repmat(sum(abs(FTS),2),1,size(FTS,2));
        FTS = sqrt(FTS);
    case 'relu_l1_sqrt_zscore'
        FTS = FTS.*(FTS > 0);
        FTS = FTS ./ repmat(sum(abs(FTS),2),1,size(FTS,2));
        FTS = sqrt(FTS);
        FTS = zscore(FTS,1);
    case 'l2'
        FTS = FTS ./ repmat(sqrt(sum(FTS.^2,2)),1,size(FTS,2));
    case 'l2_zscore'
        FTS = FTS ./ repmat(sqrt(sum(FTS.^2,2)),1,size(FTS,2));
        FTS = zscore(FTS,1);
    case 'l1_zscore'
        FTS = FTS ./ repmat(sum(abs(FTS),2),1,size(FTS,2));
        FTS = zscore(FTS,1);
    case 'l1_sqrt_zscore'
        FTS = FTS ./ repmat(sum(abs(FTS),2),1,size(FTS,2));
        FTS = sqrt(FTS);
        FTS = zscore(FTS,1);
    otherwise
        clc
        disp('No such normalization')
        return
end

end

