function defineSplits()

dataParameters = dataSetupParameters();

domainNames = dataParameters.domainNames;
featureName = dataParameters.featureName; 
mainDataDir = dataParameters.mainDataDir;
NSplits     = dataParameters.NSplits;

domainSet = defineDomainSets(domainNames);
for dd = 1 : length(domainSet)
    
    sourceDomainName = domainSet{dd}{1};
    targetDomainName = domainSet{dd}{2};
    
    load([mainDataDir '/' sourceDomainName featureName '.mat'])
    sourceLabels = LABELS';
    
    load([mainDataDir '/' targetDomainName featureName '.mat'])
    targetLabels =  LABELS';
    
    if isequal(sourceDomainName,'dslr')
        Ntr = 8;
    else
        Ntr = 20;
    end
    
    splitDetails = {};
    for split = 1 : NSplits
        
        NClasses = length(unique(LABELS'));
        
        sourceIDVec = [1 : length(sourceLabels)]';
        trainSource = [];
        testSource  = [];
        for class = 1 :  NClasses
            
           classIDs = sourceIDVec(sourceLabels==class);
           classIDs = classIDs(randperm(length(classIDs)));
        
           trainClass  = classIDs(1:Ntr); 
           trainSource = [trainSource;(trainClass+zeros(Ntr,1))];
           testSource  = [testSource ; classIDs(Ntr+1:end)];
           
        end
          
        train{split}.source = trainSource;
        train{split}.target = [];
        
        test{split}.source = testSource;
        test{split}.target = [1:length(targetLabels)]';
        
    end
    
    save([pwd '/Data/SplitDetails/' sourceDomainName '-' targetDomainName '.mat'],'train','test','dataParameters','-v7.3')
     
end


end

